### Description
This mancala's game frontend it is responsible for the actual playing of the game.

### Running
After you have cloned the project do `cd kalah && npm run start` and the React app should be running on http://localhost:3000/
this app was created with create-react-app