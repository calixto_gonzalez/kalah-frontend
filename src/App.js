import React from 'react';
import 'fontsource-roboto';
import './App.css';
import {Header} from "./Header";
import {Content} from "./Content";
import {Footer} from "./Footer";
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import Image from "./wood_grain_background.png";

const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#757ce8',
            main: '#3f50b5',
            dark: '#002884',
            contrastText: '#fff',
        },
        secondary: {
            light: '#ff7961',
            main: '#f44336',
            dark: '#ba000d',
            contrastText: '#000',
        },
    },
});
function App() {
    return (
        <div className="App">
            <ThemeProvider theme={theme}>

                <Header message="Mancala game">
                </Header>
                <img alt="" src={Image} style={{
                    position: "fixed",
                    top: 0,
                    left: 0,
                    zIndex: -1,
                    /* Preserve aspet ratio */
                    maxWidth: "1920px",
                    minHeight: "100%",
                }}/>
                <Content>

                </Content>
                <Footer>

                </Footer>
            </ThemeProvider>

        </div>
    );
}

export default App;
