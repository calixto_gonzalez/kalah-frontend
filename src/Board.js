import React from 'react';
import Image from './Board.png'
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    image: {
        backgroundImage: `url(${Image})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "100%",
    },
    [theme.breakpoints.between('xs', 'sm')]: {
        heroContainer: {
            width: theme.breakpoints.width('sm'),
            height: theme.breakpoints.width('sm') * 0.40,

            backgroundImage: `url(${Image})`,
            backgroundSize: 'cover',
            backgroundRepeat: "no-repeat",
            backgroundPosition: 'center',

        },
        box: {
            width: theme.breakpoints.width('sm') * 0.125,
            height: "100%"
        },
        innerPlayerMovementsContainer: {
            width: theme.breakpoints.width('sm') * 0.67,
            height: "100%"
        },
        rightMancala: {
            position: "relative",
            color: "#515055"
        },
        leftMancala: {
            position: "relative",
        },
    },
    [theme.breakpoints.up('md')]: {
        heroContainer: {
            width: theme.breakpoints.width('md'),
            height: theme.breakpoints.width('md') * 0.40,
            backgroundImage: `url(${Image})`,
            backgroundSize: 'cover',
            backgroundRepeat: "no-repeat",
            backgroundPosition: 'center',
            margin: "0 auto",
        },
        box: {
            width: theme.breakpoints.width('md') * 0.125,
            height: "100%"
        },
        innerPlayerMovementsContainer: {
            width: theme.breakpoints.width('md') * 0.75,
            height: "100%"
        },
        rightMancala: {
            position: "relative",
            right: theme.breakpoints.width('md') * 0.0114,
            color: "#515055"
        },
        leftMancala: {
            position: "relative",
            left: theme.breakpoints.width('md') * 0.0114,
            color: "#515055"
        },
    },
    //
    // box: {
    //     width: "125px",
    //     height: "100%"
    // },


    letterColor: {
        color: "#515055"
    }

}));
const sortAscending = (a, b) => a - b;
export function Board(props) {
    const classes = useStyles();

    const move = (place, playerType) => {
        props.makeMove({movementPlace: place, playerType: playerType});
    }

    return (
        <Grid
            container
            className={classes.heroContainer}>
            <Box
                display="flex"
                className={classes.box}
            >
                <Box m="auto">
                    <Button><Typography className={classes.leftMancala} variant="h2">{props.board[props.playerOne.bigPit]}</Typography></Button>
                </Box>
            </Box>
            <Box

                display="flex"
                className={classes.innerPlayerMovementsContainer}
            >
                <Grid container alignItems="center" justify="space-evenly" spacing={0}>
                    {props.playerOne.allowedMovements.sort(sortAscending).reverse().map(
                        p => <Grid key={"player-" + p} item xs={2}><Button onClick={() => move(p, props.playerOne.playerType)}><Typography className={classes.letterColor} variant="h2">{props.board[p]}</Typography></Button></Grid>
                    )}
                    {props.playerTwo.allowedMovements.sort(sortAscending).map(
                        p => <Grid key={"player-" + p} item xs={2}><Button onClick={() => move(p, props.playerTwo.playerType)}><Typography className={classes.letterColor} variant="h2">{props.board[p]}</Typography></Button></Grid>
                    )}
                </Grid>
            </Box>
            <Box
                display="flex"
                className={classes.box}
            >
                <Box m="auto">
                    <Typography className={classes.rightMancala} variant="h2">{props.board[props.playerTwo.bigPit]}</Typography>
                </Box>
            </Box>
        </Grid>

    );
    // <Container fixed>
    //          <Grid className={styles.image} container>
    //              <Grid container className={{padding: 120}} item xs={12}>
    //                  <Grid container alignItems="center" justify="center" spacing={5}>
    //                      <Grid item xs={2}>
    //                          <div className={styles.circle}>4</div>
    //                      </Grid>
    //                      <Grid item xs={2}>4</Grid>
    //                      <Grid item xs={2}>4</Grid>
    //                      <Grid item xs={2}>4</Grid>
    //                      <Grid item xs={2}>4</Grid>
    //                      <Grid item xs={2}>4</Grid>
    //                  </Grid>
    //              </Grid>
    //              <Grid container className={{padding: 120}} item xs={12}>
    //              </Grid>
    //          </Grid>
    //      </Container>


}