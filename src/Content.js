import React, {useEffect, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import {Board} from "./Board";
import Container from "@material-ui/core/Container";
import {makeStyles} from "@material-ui/core/styles";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from '@material-ui/lab/Alert';
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import Avatar from "@material-ui/core/Avatar";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import red from "@material-ui/core/colors/red";
import blue from "@material-ui/core/colors/blue";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";


const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
    blue: {
        color: theme.palette.getContrastText(blue[500]),
        backgroundColor: blue[500],
    },
    red: {
        color: theme.palette.getContrastText(red[500]),
        backgroundColor: red[500],
    },
}));

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const URL = "http://localhost:8080";

export function Content() {
    const [turnSnackBar, setTurnSnackBar] = useState(false);
    const [errorSnackBar, setErrorSnackBar] = useState(false);
    const [gameOverDialog, setGameOverDialog] = useState(false);
    const [restartGameDialog, setRestartGameDialog] = useState(false);
    const [playerOneData, setPlayerOneData] = useState(undefined);
    const [playerTwoData, setPlayerTwoData] = useState(undefined);
    const [playerOneScore, setPlayerOneScore] = useState(undefined);
    const [playerTwoScore, setPlayerTwoScore] = useState(undefined);
    const [playerTurn, setPlayerTurn] = useState(undefined);
    const [gameInProgressDialog, setGameInProgressDialog] = useState(false);
    const [continueGame, setContinueGame] = useState(false);
    const [id, setId] = useState(undefined);
    const [board, setBoard] = useState(undefined);
    const [errorSnackBarMessage, setErrorSnackBarMessage] = useState("");
    const [turnSnackBarMessage, setTurnSnackBarMessage] = useState("");
    const classes = useStyles();


    function isGameInProgress() {
        if (localStorage.getItem('gameId')) {
            setId(localStorage.getItem('gameId'));
            setGameInProgressDialog(true);
        }
    }

    function fetchGameById() {
        if (continueGame) {
            fetch(`${URL}/game/${id}`)
                .then(value => {
                    value.json().then((data) => {
                        if (data.error)
                            throw new Error(data.error);
                        setVariablesFromResponse(data)
                    })
                })
                .catch(error => {
                    setErrorSnackBarMessage(error.message);
                    setErrorSnackBar(true);
                })
        }
    }

    useEffect(isGameInProgress, []);
    useEffect(fetchGameById, [continueGame]);


    function getMessage(response) {
        if (response.status.gameOver) {
            return 'Game is over';
        }
        return `It is Player's ${response.status.whoPlays === 'NORTH_PLAYER' ? 'One' : 'Two'} turn`;
    }

    function setVariablesFromResponse(jsonResponse) {
        const playerOne = jsonResponse.players.filter(p => p.playerType === "NORTH_PLAYER")[0];
        const playerTwo = jsonResponse.players.filter(p => p.playerType === "SOUTH_PLAYER")[0];
        setPlayerOneData(playerOne);
        setPlayerTwoData(playerTwo);
        setPlayerOneScore(playerOne.score);
        setPlayerTwoScore(playerTwo.score);
        setBoard(jsonResponse.board);
        setId(jsonResponse.id);
        setPlayerTurn(jsonResponse.status.whoPlays);
        setTurnSnackBar(true);
        setTurnSnackBarMessage(getMessage(jsonResponse));
        setGameOverDialog(jsonResponse.status.gameOver);
        localStorage.setItem('gameId', jsonResponse.id);
    }

    const fetchGame = function (event) {
        fetch(`${URL}/game`)
            .then(value => {
                value.json().then((data) => {
                    if (data.error)
                        throw new Error(data.error);
                    setVariablesFromResponse(data)
                })
            })
            .catch(error => {
                setErrorSnackBarMessage(error.message);
                setErrorSnackBar(true);
            })
    };


    const handleOpenDialog = (event) => {
        setRestartGameDialog(true);
    };


    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setTurnSnackBar(false);
        setErrorSnackBar(false);
    };


    const handleMovement = (playerObject) => {
        fetch(`${URL}/game/${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                ...playerObject,
                id: id
            })
        })
            .then(response => response.json().then(data => {
                if (data.error)
                    throw new Error(data.error);
                setVariablesFromResponse(data)
            })).catch(error => {
            setErrorSnackBarMessage(error.message);
            setErrorSnackBar(true);
        })
    };
    const turnMessage = "It is your turn!";

    return (
        <Container className={classes.root} fixed>
            <Grid container spacing={3}
                  direction="row"
                  justify="center"
                  alignItems="center"
            >
                <Grid container item xs={12} justify="flex-start">

                    <Card>
                        <CardHeader
                            avatar={<Avatar className={classes.blue}>P1</Avatar>}
                            title="Player 1"
                            subheader={playerOneScore ? "Score: " + playerOneScore : ""}
                        />
                        <CardContent>
                            <Typography gutterBottom variant="body2" component="p">{playerTurn === 'NORTH_PLAYER' ? turnMessage : ""}</Typography>

                        </CardContent>
                    </Card>
                </Grid>
                <Grid container item xs={12}>
                    {playerTwoData && playerOneData && board ?
                        <Board
                            playerOne={playerOneData}
                            playerTwo={playerTwoData}
                            board={board}
                            makeMove={handleMovement}/> : ""}
                </Grid>
                {id ?
                    <Grid container item xs={6} justify="flex-start">
                        <Button onClick={handleOpenDialog} variant="contained" color="primary">Restart Game</Button>

                    </Grid>
                    :
                    <Grid container item xs={6} justify="flex-start">
                        <Button onClick={fetchGame} variant="contained" color="primary">Start Game</Button>

                    </Grid>
                }

                <Grid container item xs={6} justify="flex-end">
                    <Card>
                        <CardHeader
                            avatar={<Avatar className={classes.red}>P2</Avatar>}
                            title="Player 2"
                            subheader={playerTwoScore ? "Score: " + playerTwoScore : ""}
                        />
                        <CardContent>
                            <Typography gutterBottom variant="body2" component="p">{playerTurn === 'SOUTH_PLAYER' ? turnMessage : ""}</Typography>
                        </CardContent>
                    </Card>

                </Grid>


            </Grid>
            <Snackbar open={turnSnackBar} autoHideDuration={5000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success">
                    {turnSnackBarMessage}
                </Alert>
            </Snackbar>
            <Snackbar open={errorSnackBar} autoHideDuration={5000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="error">
                    {errorSnackBarMessage}
                </Alert>
            </Snackbar>
            <Dialog
                open={gameOverDialog}
                // onClose={handleCloseDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">Game Over</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Game is over Player {playerOneScore > playerTwoData ? "One" : "Two"} has won!!
                        Do you wish to start a new one?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={(event) => {
                        setGameOverDialog(false);
                        fetchGame(event);
                    }} color="primary" autoFocus>
                        Yes
                    </Button>
                    <Button onClick={() => setGameOverDialog(false)} color="primary" autoFocus>
                        No
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog
                open={restartGameDialog}
                // onClose={handleCloseDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">Restart Game</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Are you sure you want to restart game?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={(event) => {
                        setRestartGameDialog(false);
                        fetchGame(event);
                    }} color="primary" autoFocus>
                        Yes
                    </Button>
                    <Button onClick={() => setRestartGameDialog(false)} color="primary" autoFocus>
                        No
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog
                open={gameInProgressDialog}
                // onClose={handleCloseDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">Game in progress</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        There is already a game in progress would you like to resume?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={(event) => {
                        setGameInProgressDialog(false);
                        setContinueGame(true);
                    }} color="primary" autoFocus>
                        Yes
                    </Button>
                    <Button onClick={(event) => {
                        setRestartGameDialog(false);
                        setContinueGame(true);
                    }} color="primary" autoFocus>
                        No
                    </Button>
                </DialogActions>
            </Dialog>
        </Container>
    )

}