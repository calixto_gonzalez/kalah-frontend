import React from 'react';

export function Header(props) {
    return (
        <div className="Title">
            <header>
                <h1>{props.message}</h1>
            </header>
        </div>
    )
}